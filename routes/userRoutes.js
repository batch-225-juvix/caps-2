// // ======== INSTALLERS FOR ROUTES ======== //

const express = require("express");
const router = express.Router();
const auth = require("../auth");

// ================================= //



// ============== IMPORT =================================== //

const userController = require("../controllers/userController")

// =========================================================== //


// ============== LOCALHOST URL FOR REGISTRATION ===================== //

router.post("/register", (req, res) => {

     userController.registerUser(req.body).then(
         
          result => res.send(result))

});

// =================================================================== //




// ============== LOCALHOST URL FOR CHECK EMAIL ==================== //

router.post("/checkEmail", (req, res) => {
     userController.checkEmailExists(req.body).then(result => res.send(result));
});

// =================================================================== //








// ======= LOCALHOST URL FOR LOGIN/AUTHENTICATION ==================== //

router.post("/login", (req, res) => {

     userController.loginUser(req.body).then(result => res.send(result));
})

// ============================================================ //







// ============== LOCALHOST URL FOR PRODUCT =============== //

router.post("/product", auth.verify, (req, res) => {

     let data = {
       // user ID will be retrieved from the request header
       userId : auth.decode(req.headers.authorization).id,
       
       // Course ID will be retrieve from the request body
       courseId : req.body.courseId
     }
   
     userController.enroll(data).then(result => res.send(result));
   
   })

// =============================================================== //



// ============== LOCALHOST URL FOR archiveProduct =============== //

router.post("/archiveProduct", auth.verify, (req, res) => {

     let data = {
       // user ID will be retrieved from the request header
       userId : auth.decode(req.headers.authorization).id,
       
       // Course ID will be retrieve from the request body
       courseId : req.body.courseId
     }
   
     userController.enroll(data).then(result => res.send(result));
   
   })

// =============================================================== //





// ==== CONSOLE LOG ====== //

module.exports = router;

// ============================ //