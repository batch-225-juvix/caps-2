
// ====== INSTALLERS ============== //
const express = require("express");
const router = express.Router();
const auth = require("../auth");

// ================================= //



// ==================Connection to the productControllers.js=================== //
// for directory
const productController = require("../controllers/productController");
// ============================================= //



// ============== LOCALHOST URL FOR PRODUCT =============== //

router.post("/product", auth.verify, (req, res) => {

     let data = {
       // user ID will be retrieved from the request header
       userId : auth.decode(req.headers.authorization).id,
       
       // Course ID will be retrieve from the request body
       courseId : req.body.courseId
     }
   
     userController.enroll(data).then((result) => res.send(result));
   
   })

// =============================================================== //



// ============== CREATE / ADD PRODUCT ===================== //
router.post("/addProduct", auth.verify, (req, res) => {


	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productController.addProduct(data).then((result) => res.send(result));
})
// ================================================= //








// ========= RETRIEVE ALL PRODUCT (ADMIN ONLY) ============= //

router.get("/retrieveProducts", auth.verify, (req, res) => {

 const data = {
        product: req.body,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
 }   
    productController.getAllProducts(data).then((result) => res.send(result));
   
  });
// ============================================= //





//============  RETRIEVE ALL ACTIVE PRODUCT ====================== //

router.get("/retrieveAllActiveProducts", auth.verify, (req, res) => {

 const data = {
        product: req.body,
        isActive: true
 }   
    productController.getAllActiveProducts(data).then((result) => res.send(result));
   
  });


// ===================================================================== //







// === router for users in getting product (USERS ONLY) =================//

router.get("/getAllActiveProduct", auth.verify, (req,res) => {

    productController.getAllActiveProduct().then((result) => res.send(result));
} );

// ====================================================================== //












// ======== RETRIEVE SINGLE PRODUCT ===============!!!! //


router.post("/retrieveSingleProduct", auth.verify, (req, res) => {
  const data = {
    id: req.body,
    isAdmin: auth.decode(req.headers.authorization).isAdmin,
  };
  productController.retrieveSingleProduct(data).then((result) => res.send(result));
});


// ================================================= //








// ======== UPDATE PRODUCT I.D. (ADMIN ONLY) ===================== //
router.put("/:productId", auth.verify, (req, res) => {

	productController.updateProducts(req.params, req.body).then((result) => res.send(result));
});
// ================================================ //









// ======= ROUTES FOR ARCHIVING A PRODUCT (SOFT DELETE ITEMS)============ //

router.put("/archiveProduct/:productId", auth.verify, (req, res) => {

  productController.archiveProduct(req.params).then(
    (result) => res.send(result)
  );
});
// ================================================= //





// ======= ROUTES FOR UNARCHIVING A PRODUCT (RESTORE DELETE ITEMS)============ //

router.put("/unarchiveProduct/:productId", auth.verify, (req, res) => {

  productController.unarchiveProduct(req.params).then(
    (result) => res.send(result)
  );
});
// ================================================= //










// =========== Non Admin User CheckOut (Create Order) ========= //

router.get("/userCheckout", auth.verify, (req, res) => {

  productController.userCheckout(req.params).then(

    (result) => res.send(result)
    );
});




// =============================================================== //












// ============= Retrieve User Details ========================= //

router.get("/retrieveUserDetails", auth.verify, (req, res) => {

  productController.retrieveUserDetails(req.params).then(

    (result) => res.send(result)
    );
});



// ============================================================== //







// === console log ========== //
module.exports = router;
// ========================== //
