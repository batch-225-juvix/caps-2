 const Product = require("../models/product");

// =================================== ADD PRODUCT ================================================= //


module.exports.addProduct = (data) => {
  if (data.isAdmin) {
    return Product.findOne({ productName: data.product.productName }).then(
      (product) => {
        if (product) {
          let message = Promise.resolve("Product already added!");
          return message.then((value) => {
            return value;
          });
        } else {
          let new_product = new Product({
            productName: data.product.productName,
            productDescription: data.product.productDescription,
            price: data.product.price,
            createdOn: data.product.createdOn,
          });
          return new_product.save().then((new_product, error) => {
            if (error) {
              return error;
            }
            let message = Promise.resolve("New Product Successfully Added.");
            return message.then((value) => {
              return value;
            });
          });
        }
      }
    );
  } else {
    let message = Promise.resolve("User must be Admin to Access this.");
    return message.then((value) => {
      return value;
    });
  }
};
// ======================================================================================= // 








// =============== Retrieve / GET ALL PRODUCTS ======== //

module.exports.getAllProducts = (data) => {
    if(data.isAdmin){
	return Product.find({}).then(result => {
   		 return result;
    });
};
		let message = Promise.resolve('User must be Admin to Access this.')

		return message.then((value) => {
			return value
	});		

}

// =================================================== //






// =============== Retrieve / GET ALL ACTIVE PRODUCTS ======== //

module.exports.getAllActiveProducts = (data) => {
    
	return Product.find({isActive: true}).then(result => {
   		 return result;
    });

}

// ==================================================== //








// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! //
// ================ Retrieve single Product =================== //


module.exports.retrieveSingleProduct = (data) => {
  if (data.isAdmin) {
    return Product.findById(data.id).then((product) => {
      return product;
    });
  }
  let message = Promise.resolve("User must be Admin to use this feature.");

  return message.then((value) => {
    return value;
  });
};



// ============================================================ //
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! //









// ========== UPDATE PRODUCT information (Admin only) ======================================= //

module.exports.updateProducts = (reqParams, reqBody) => {

	// specify the fields/properties of the document to be updated


	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};


		return Product.findByIdAndUpdate(reqParams.productId,updatedProduct).then((course, error) => {

				if (error) {

					return false;

				} else {

					return {
						message: "Product updated successfully"
					}
				};


		});

};
// ================================================================================================== //









// ===============================ARCHIVE PRODUCT ( Admin Only )============================================================= //

module.exports.archiveProduct = (reqParams) => {

	let updateActiveField = {
		
		isActive : false
	};

		return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {
			if (error) {

				return false;
			} else {
				return {
					message : "Archiving Product successfully"
				}
			};
		});

};
// ================================================================================================================ //








// =============================== UNARCHIVE PRODUCT ( Admin Only ) ============================================================= //

module.exports.unarchiveProduct = (reqParams) => {

	let updateActiveField = {
		
		isActive : true
	};

		return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {
			if (error) {

				return false;
			} else {
				return {
					message : "Unarchiving Product successfully"
				}
			};
		});

};
// ================================================================================================================ //








// ========== Non Admin User Checkout ( Create Order ) ====================== //

module.exports.userCheckOut = (data) => {

	return Product.findById(data.product.productId).then(product => {

		if(data.isAdmin){

			let message = Promise.resolve({
					message: 'Admin cannot place an order'
			})
			return message.then((value) => {
				return value
			})
		}


			let newOrder = new Order({
					userId: data.userId,
					products: [{
							productId: data.product.productId,
							quantity: data.product.quantity
					}],
					totalAmount: product.price * data.product.quantity
			});

				return newOrder.save().then(result => {
					return Promise.resolve({
								message: "Order successfully created",
								result: result
					})
			})

	})
};



// ============================================================================ //







// Retrieve User Details
// =================== RETRIEVE ACTIVE PRODUCTS ================================== //


module.exports.retrieveUserDetails = () => {

	 return Product.find({isActive : true}).then(result => {
	 	return result;
	 }) ;
};
// ====================================================================== //
