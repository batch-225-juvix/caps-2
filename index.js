// ===== INSTALLERS ======== //

const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const dotenv = require("dotenv").config();

// ========================================  //





// ==== CONNECT TO ROUTES FOLDER ======== //

const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes")
const orderRoutes = require("./routes/orderRoutes")
// ======================================== //







//  ======= CONNECT to EXPRESS ======== //

const app = express();
const port = 3010;
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// ========================================== //









// ======= CONNECT MONGOOSE =================================== //

mongoose.connect(`mongodb+srv://juvix45:${process.env.PASSWORD}@cluster0.67sy6wx.mongodb.net/Ecommerce-API?retryWrites=true&w=majority`, 
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }
);

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));

db.on("open", () => console.log("Connected to MongoDB."));

// ======================================================= //






// ========= LOCALHOST URL ROUTES ======== //

app.use("/users", userRoutes);
app.use("/product", productRoutes);
app.use("/order", orderRoutes);

// ==================================== //






// ======== CONSOLE LOG PORT ========== //

app.listen(port, () => console.log(`API is now online on port ${port}`));

// =========================================== // 